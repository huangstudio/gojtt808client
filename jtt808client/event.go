package jtt808client

// 回调事件接口
type EventHandler interface {
	// 通知事件
	Notify(data *GatewayData)
}
