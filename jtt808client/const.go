package jtt808client

const (
	BufferSize    = 1024 * 10         // 缓冲区大小
	MaxBufferSize = 1024 * 100        // 最大缓冲区大小
	GFrameHeader  = 0x02020202        // 帧头
	GFrameEnd     = 0x03030303        // 帧尾
	ClientType    = "jtt808client"    // 客户端类型
	NetType       = "tcp"             // 网络类型
	Login         = "login"           // 登录
	Subcribe      = "subcribe"        // 订阅
	Transparent   = "808_transparent" // 808透传
	Command808    = "808_command"     // 808命令
)

var (
	GFrameHeaderBytes = []byte{0x02, 0x02, 0x02, 0x02} // 帧头字节数组
	GFrameEndBytes    = []byte{0x03, 0x03, 0x03, 0x03} // 帧尾字节数组
	RegionType        = map[byte]uint16{
		1: 0x8601, // 圆形区域
		2: 0x8603, // 矩形区域
		3: 0x8605, // 多边形区域
		4: 0x8607, // 线路区域
	} // 区域类型
)
