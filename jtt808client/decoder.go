package jtt808client

import (
	"encoding/json"
	"net"
)

// 解码器回调映射表
var MsgEncodeFuncMap = map[string]MsgEncodeFunc{
	"login": login,
}

// 解码器回调函数r
type MsgEncodeFunc func(conn net.Conn, data *GatewayData)

// 客户端登录响应
func login(conn net.Conn, data *GatewayData) {
	// 先将消息体序列化为json字符串
	bytes, err := json.Marshal(data.Body)
	if err != nil {
		return
	}
	loginResultrData := &loginResult{}
	err = json.Unmarshal(bytes, loginResultrData)
	if err != nil {
		return
	}
	if loginResultrData.Code != 0 {
		Client().connectStatus = false
		Client().CanWrite = false
		// 登录失败，关闭连接
		conn.Close()

		return
	}
	// 登录成功，可写入数据
	Client().CanWrite = true
}
