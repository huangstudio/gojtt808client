package jtt808client

import "encoding/binary"

// 读取uint32
func ReadUint32(buffer []byte, idx *int) uint32 {
	if *idx > len(buffer) {
		*idx += 4
		return 0
	}
	if *idx+4 > len(buffer) {
		*idx += 4
		return 0
	}
	it := binary.BigEndian.Uint32(buffer[*idx : *idx+4])
	*idx += 4
	return it
}
