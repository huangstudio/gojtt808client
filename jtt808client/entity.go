package jtt808client

// 消息头
type MsgHeader struct {
	MsgId       uint16 `json:"msgId"`       // 消息ID
	MsgLen      uint16 `json:"msgLen"`      // 消息长度
	Encrypt     bool   `json:"encrypt"`     // 加密标识
	Package     bool   `json:"package"`     // 分包标识
	VersionFlag bool   `json:"versionFlag"` // 版本标识
	Version     byte   `json:"version"`     // 软件版本号
	Phone       string `json:"phone"`       // 终端手机号
	MsgSerial   uint16 `json:"msgSerial"`   // 消息流水号
	TotalPkg    uint16 `json:"totalPkg"`    // 消息包总数
	CurrentPkg  uint16 `json:"currentPkg"`  // 当前消息包序号
}

// 消息体
type MsgBody interface{}

// 网关数据
type GatewayData struct {
	ClientType string    `json:"clientType"` // 客户端类型, 如: "jtt808server"、"jtt808client"等
	MsgType    string    `json:"msgType"`    // 消息类型, 如: "login"等
	NetType    string    `json:"netType"`    // 网络类型
	Addr       string    `json:"addr"`       // 客户端地址
	ServerAddr string    `json:"serverAddr"` // 服务端地址
	RawHex     string    `json:"rawHex"`     // 原始十六进制数据
	Header     MsgHeader `json:"header"`     // 消息头
	Body       MsgBody   `json:"body"`       // 消息体
}

// 客户端登录
type loginRequest struct {
	UserName          string   `json:"userName"`          // 用户名
	Password          string   `json:"password"`          // 密码
	UserType          string   `json:"userType"`          // 用户类型，web:后端，client:桌面客户端，mobile:手机，809:政府监管怕平台，1078：部标视频，third：第三方，other：其他， 默认为web
	TerminalPhoneList []string `json:"terminalPhoneList"` // 终端手机号列表
	Subscriptions     []string `json:"subscriptions"`     // 订阅消息类型列表
}

// 客户端登录结果
type loginResult struct {
	Code   int    `json:"code"`   // 登录结果码，0表示成功，1表示失败
	Result string `json:"result"` // 登录结果
}

// 订阅消息
type subcribeRequest struct {
	TerminalPhoneList []string `json:"terminalPhoneList"` // 终端手机号列表
	Subscriptions     []string `json:"subscriptions"`     // 订阅消息类型列表
}

// 订阅808透传
type TransparentReq struct {
	ServiceId         int      `json:"serviceId"`         // 服务id
	Addr              string   `json:"addr"`              // 目标地址
	TerminalPhoneList []string `json:"terminalPhoneList"` // 终端手机号列表
	ProtocolType      string   `json:"protocolType"`      // 协议类型：JTT2013、JTT2019
	Subscriptions     []string `json:"subscriptions"`     // 订阅消息类型列表
	StartStatus       int      `json:"startStatus"`       // 启动状态，0：停止，1：启动
}

// 终端控制
type TerminalControlRequest struct {
	Cmd   int    `json:"cmd"`   // 命令字，1-7，对应不同的命令，具体参数见终端控制命令字表
	Value string `json:"value"` // 参数值
}

// 人工确认报警消息
type AckAlarmRequest struct {
	MsgSerial uint16 `json:"msgSerial"` // 需人工确认的报警消息流水号，0 表示该报警类型所有消息
	AlarmType uint32 `json:"alarmType"` // 报警类型，见JTT808协议中定义
}

// 临时位置跟踪
type TrackRequest struct {
	Interval uint16 `json:"interval"` // 时间间隔，单位为秒，0 则停止跟踪。停止跟踪无需带后继字段
	Duration uint32 `json:"duration"` // 位置跟踪有效期，单位为秒，终端在接收到位置跟踪控制消息后，在有效期截止时间之前，依据消息中的时间间隔发送位置汇报
}

// 文本信息下发
type TextRequest struct {
	TextFlag byte   `json:"textFlag"` // 文本信息标志位，有两种的定义，参见JTT808-2013或2019协议中定义
	TextType byte   `json:"textType"` // 文本信息类型，参见JTT808协议中定义，1：通知，2：服务（仅2019版支持，2013版没有这个字段，默认为0）
	Text     string `json:"text"`     // 文本信息内容，最长为 1024 字节，经 GBK 编码，最大支持500个汉字
}

// 电话回拨
type PhoneCallBackRequest struct {
	CallType int    `json:"callType"` // 呼叫类型，0:普通通话；1:监听
	Phone    string `json:"phone"`    // 电话号码，最长为 20 字节
}

// 设置电话本
type SetPhoneBookRequest struct {
	SetType int             `json:"setType"` // 设置类型，0：删除终端上所有存储的联系人； 1：表示更新电话本（删除终端中已有全部联系人并追加消息中的联系人）； 2：表示追加电话本； 3：表示修改电话本（以联系人为索引）
	Books   []PhoneContacts `json:"phone"`   // 联系人项
}

// 电话本联系人
type PhoneContacts struct {
	CallType int    `json:"callType"` // 呼叫类型，1：呼入；2：呼出；3：呼入/呼出
	Phone    string `json:"phone"`    // 电话号码
	Contacts string `json:"contacts"` // 联系人
}

// 区域
type Region struct {
	OperateType byte `json:"operateType"` // 操作类型，0：更新区域； 1：追加区域； 2：修改区域
}

// 圆形区域
type CircleRegion struct {
	Region
	RegionItems []CircleRegionItem `json:"regionItems"` // 圆形区域项
}

// 圆形区域项
type CircleRegionItem struct {
	Id            uint32 `json:"regionId"`      // 区域ID
	Attribute     uint16 `json:"attribute"`     // 区域属性，见JTT808协议中定义
	Latitude      uint32 `json:"latitude"`      // 纬度，单位为度，乘以 10^6，WGS84坐标系
	Longitude     uint32 `json:"longitude"`     // 经度，单位为度，乘以 10^6，WGS84坐标系
	Radius        uint32 `json:"radius"`        // 半径，单位为米
	StartTime     string `json:"startTime"`     // 开始时间，格式为：YYYY-MM-DD HH:mm:ss，若区域属性0位为0则没有该字段
	EndTime       string `json:"endTime"`       // 结束时间，格式为：YYYY-MM-DD HH:mm:ss，若区域属性0位为0则没有该字段
	MaxSpeed      uint16 `json:"maxSpeed"`      // 最高速度，单位为千米/小时，若区域属性1位为0则没有该字段
	Duration      byte   `json:"duration"`      // 持续时间，单位为秒，若区域属性1位为0则没有该字段
	NightMaxSpeed uint16 `json:"nightMaxSpeed"` // 夜间最高速度（2019版支持），单位为千米/小时，若区域属性1位为0则没有该字段
}

// 矩形区域
type RectangRegion struct {
	Region
	RegionItems []RectangRegionItem `json:"regionItems"` // 矩形区域项
}

// 矩形区域项
type RectangRegionItem struct {
	Id             uint32 `json:"regionId"`       // 区域ID
	Attribute      uint16 `json:"attribute"`      // 区域属性，见JTT808协议中定义
	LeftLatitude   uint32 `json:"leftLatitude"`   // 纬度，单位为度，乘以 10^6，WGS84坐标系
	LeftLongitude  uint32 `json:"leftLongitude"`  // 经度，单位为度，乘以 10^6，WGS84坐标系
	RightLatitude  uint32 `json:"rightLatitude"`  // 纬度，单位为度，乘以 10^6，WGS84坐标系
	RightLongitude uint32 `json:"rightLongitude"` // 经度，单位为度，乘以 10^6，WGS84坐标系
	StartTime      string `json:"startTime"`      // 开始时间，格式为：YYYY-MM-DD HH:mm:ss，若区域属性0位为0则没有该字段
	EndTime        string `json:"endTime"`        // 结束时间，格式为：YYYY-MM-DD HH:mm:ss，若区域属性0位为0则没有该字段
	MaxSpeed       uint16 `json:"maxSpeed"`       // 最高速度，单位为千米/小时，若区域属性1位为0则没有该字段
	Duration       byte   `json:"duration"`       // 持续时间，单位为秒，若区域属性1位为0则没有该字段
	NightMaxSpeed  uint16 `json:"nightMaxSpeed"`  // 夜间最高速度（2019版支持），单位为千米/小时，若区域属性1位为0则没有该字段
}

// 多边形区域
type PolygonRegion struct {
	Id            uint32   `json:"regionId"`      // 区域ID
	Attribute     uint16   `json:"attribute"`     // 区域属性，见JTT808协议中定义
	StartTime     string   `json:"startTime"`     // 开始时间，格式为：YYYY-MM-DD HH:mm:ss，若区域属性0位为0则没有该字段
	EndTime       string   `json:"endTime"`       // 结束时间，格式为：YYYY-MM-DD HH:mm:ss，若区域属性0位为0则没有该字段
	MaxSpeed      uint16   `json:"maxSpeed"`      // 最高速度，单位为千米/小时，若区域属性1位为0则没有该字段
	Duration      byte     `json:"duration"`      // 持续时间，单位为秒，若区域属性1位为0则没有该字段
	VertexItems   []Vertex `json:"vertexItems"`   // 多边形区域顶点列表
	NightMaxSpeed uint16   `json:"nightMaxSpeed"` // 夜间最高速度（2019版支持），单位为千米/小时，若区域属性1位为0则没有该字段
}

// 多边形区顶点
type Vertex struct {
	Latitude  uint32 `json:"latitude"`  // 纬度，单位为度，乘以 10^6，WGS84坐标系
	Longitude uint32 `json:"longitude"` // 经度，单位为度，乘以 10^6，WGS84坐标系
}

// 线路区域
type LineRegion struct {
	Id                   uint32            `json:"regionId"`             // 线路ID
	Attribute            uint16            `json:"attribute"`            // 线路属性，见JTT808协议中定义
	StartTime            string            `json:"startTime"`            // 开始时间，格式为：YYYY-MM-DD HH:mm:ss，若区域属性0位为0则没有该字段
	EndTime              string            `json:"endTime"`              // 结束时间，格式为：YYYY-MM-DD HH:mm:ss，若区域属性0位为0则没有该字段
	InflectionPointItems []InflectionPoint `json:"inflectionPointItems"` // 线路拐点列表
}

// 线路拐点项
type InflectionPoint struct {
	IpId           uint32 `json:"ipId"`           // 拐点ID
	SectionId      uint32 `json:"sectionId"`      // 路段ID
	Latitude       uint32 `json:"latitude"`       // 纬度，单位为度，乘以 10^6，WGS84坐标系
	Longitude      uint32 `json:"longitude"`      // 经度，单位为度，乘以 10^6，WGS84坐标系
	Width          byte   `json:"width"`          // 路段宽度，单位为米（m），路段为该拐点到下一拐点
	Attribute      byte   `json:"attribute"`      // 路段属性，见JTT808协议中定义
	DrivingTooLong uint16 `json:"drivingTooLong"` // 路段行驶过长阈值，单位为秒，若路段属性0位为0则没有该字段
	DrivingLack    uint16 `json:"drivingLack"`    // 路段行驶不足阈值，单位为秒，若路段属性0位为0则没有该字段
	MaxSpeed       uint16 `json:"maxSpeed"`       // 路段最大速度，单位为千米/小时，若路段属性1位为0则没有该字段
	Duration       byte   `json:"duration"`       // 路段持续时间，单位为秒，若路段属性1位为0则没有该字段
	NightMaxSpeed  uint16 `json:"nightMaxSpeed"`  // 夜间最高速度（2019版支持），单位为千米/小时，若区域属性1位为0则没有该字段
}

// 多媒体数据上传应答
type AckMediaDataUploadRequest struct {
	MediaId uint32   `json:"mediaId"` // 多媒体 ID，>0，如收到全部数据包则没有后续字段
	Ids     []uint16 `json:"ids"`     // 重传包 ID 列表，重传包序号顺序排列，如“包 ID1 包 ID2......包 IDn”。
}

// 拍照请求
type TakePhotoRequest struct {
	Channel    byte   `json:"channel"`    // 通道号
	Cmd        uint16 `json:"cmd"`        // 拍照命令，0 表示停止拍摄；0xFFFF 表示录像；其它表示拍照张数
	Interval   uint16 `json:"interval"`   // 拍照间隔/录像时间，单位为秒，0 表示按最小间隔拍照或一直录像
	SaveFlag   byte   `json:"saveFlag"`   // 保存标志，0：实时上传，1：保存
	Resolution byte   `json:"resolution"` // 分辨率，1:320*240； 2:640*480；3:800*600；4:1024*768; 5:176*144;[Qcif]; 6:352*288;[Cif]; 7:704*288;[HALF D1]; 8:704*576;[D1];
	Quality    byte   `json:"quality"`    // 图像/视频质量，1-10，1 代表质量损失最小，10 表示压缩比最大
	Brightness byte   `json:"brightness"` // 亮度，0-255
	Contrast   byte   `json:"contrast"`   // 对比度，0-127
	Saturation byte   `json:"saturation"` // 饱和度，0-127
	Hue        byte   `json:"hue"`        // 色度，0-255
}

// 存储多媒体数据检索
type QueryMediaDataRequest struct {
	MediaType  byte   `json:"mediaType"`  // 多媒体类型，0：图片，1：音频，2：视频
	Channel    byte   `json:"channel"`    // 通道号
	MediaEvent byte   `json:"mediaEvent"` // 多媒体事件，0：平台下发指令；1：定时动作；2：抢劫报警触发；3：碰撞侧翻报警触发；其他保留
	StartTime  string `json:"startTime"`  // 开始时间，格式为：YYYY-MM-DD HH:mm:ss
	EndTime    string `json:"endTime"`    // 结束时间，格式为：YYYY-MM-DD HH:mm:ss
}

// 存储多媒体数据上传命令
type UploadMediaDataRequest struct {
	MediaType  byte   `json:"mediaType"`  // 多媒体类型，0：图片，1：音频，2：视频
	Channel    byte   `json:"channel"`    // 通道号
	MediaEvent byte   `json:"mediaEvent"` // 多媒体事件，0：平台下发指令；1：定时动作；2：抢劫报警触发；3：碰撞侧翻报警触发；其他保留
	StartTime  string `json:"startTime"`  // 开始时间，格式为：YYYY-MM-DD HH:mm:ss
	EndTime    string `json:"endTime"`    // 结束时间，格式为：YYYY-MM-DD HH:mm:ss
	DeleteFlag byte   `json:"deleteFlag"` // 删除标志，0：保留，1：删除
}

// 录音开始命令
type SoundRecordingRequest struct {
	Cmd      byte   `json:"cmd"`      // 录音命令，0：停止录音；1：开始录音
	Duration uint16 `json:"duration"` // 录音时长，单位为秒，0 表示一直录音
	SaveFlag byte   `json:"saveFlag"` // 保存标志，0：实时上传，1：保存
	Rate     byte   `json:"rate"`     // 采样率，0：8K，1：11KHz，2：23K，3：32KHz;其他保留
}

// 单条存储多媒体数据检索上传命令
type QueryMediaDataSingleRequest struct {
	MediaId    uint32 `json:"mediaId"`    // 多媒体ID，>0
	DeleteFlag byte   `json:"deleteFlag"` // 删除标志，0：保留，1：删除
}

// 数据下行透传
type DataDownTransRequest struct {
	MsgTypeId byte   `json:"msgTypeId"` // 透传消息类型ID，见JTT808协议中定义
	DataType  byte   `json:"dataType"`  // 数据类型，0：ASCII码字符串，1：16进制字符串
	Data      string `json:"data"`      // 数据
}

// 实时音视频请求
type RealTimeVideoRequest struct {
	Ip         string `json:"ip"`         // 服务器IP地址
	PortTcp    uint16 `json:"portTcp"`    // 服务器TCP端口号
	PortUdp    uint16 `json:"portUdp"`    // 服务器UDP端口号
	Channel    byte   `json:"channel"`    // 通道号
	DataType   byte   `json:"dataType"`   // 数据类型，0：音视频，1：视频，2：双向对讲，3：监听，4：中心广播，5：透传
	StreamType byte   `json:"streamType"` // 码流类型，0：主码流，1：子码流
}

// 实时音视频控制
type RealTimeVideoControlRequest struct {
	Channel    byte `json:"channel"`    // 通道号
	CtlType    byte `json:"ctlType"`    // 控制类型，0：关闭音视频，1：切换码流，2：暂停该通道所有流的发送，3：恢复暂停前流的发送，4：关闭双向对讲
	CloseType  byte `json:"closeType"`  // 关闭类型，0：关闭音视频，1：关闭音频，2：关闭视频
	StreamType byte `json:"streamType"` // 码流类型，0：主码流，1：子码流
}

// 实时音视频传输状态通知
type RealTimeVideoStatusNotifyRequest struct {
	Channel  byte `json:"channel"`  // 通道号
	LossRate byte `json:"lossRate"` // 丢包率，当前传输通道的丢包率，数值乘以100之后取整数部分
}

// 远程录像回放请求
type HistoryVideoRequest struct {
	Ip          string `json:"ip"`          // 服务器IP地址
	PortTcp     uint16 `json:"portTcp"`     // 服务器TCP端口号
	PortUdp     uint16 `json:"portUdp"`     // 服务器UDP端口号
	Channel     byte   `json:"channel"`     // 通道号
	DataType    byte   `json:"dataType"`    // 音视频类型，0：音视频，1：音频，2：视频，3：视频或音频
	StreamType  byte   `json:"streamType"`  // 码流类型，0：主码流或子码流，1：主码流，2：子码流，如果是音频数据，该字段赋值为0
	StorageType byte   `json:"storageType"` // 存储类型，0：主存储器或灾备存储器，1：主存储器，2：灾备存储器
	Playback    byte   `json:"playback"`    // 回放方式，0：正常回放，1：快进回放，2：关键帧快退回放，3：关键帧播放，4：单帧上传
	Multiple    byte   `json:"multiple"`    // 快进或快退倍数，放回方式为1和2时，此字段内容有效，否则置为0，0：无效，1：1倍，2：2倍，3：4倍，4：8倍，5：16倍
	StartTime   string `json:"startTime"`   // 开始时间，格式为：YYYY-MM-DD HH:mm:ss
	EndTime     string `json:"endTime"`     // 结束时间，格式为：YYYY-MM-DD HH:mm:ss
}

// 远程录像回放控制
type HistoryVideoControlRequest struct {
	Channel   byte   `json:"channel"`   // 通道号
	CtlType   byte   `json:"ctlType"`   // 回放控制，0：开始回放，1：暂停回放，2：结束回放，3：快进回放，4：关键帧快退回放，5：拖动回放，6：关键帧播放
	Multiple  byte   `json:"multiple"`  // 快进或快退倍数，放回方式为3和4时，此字段内容有效，否则置为0，0：无效，1：1倍，2：2倍，3：4倍，4：8倍，5：16倍
	StartTime string `json:"startTime"` // 拖动回放时间，格式为：YYYY-MM-DD HH:mm:ss，回放控制为5时，此字段有效
}

// 查询音视频资源列表
type AudioVideoResourceRequest struct {
	Channel      byte   `json:"channel"`      // 通道号
	StartTime    string `json:"startTime"`    // 开始时间，格式为：YYYY-MM-DD HH:mm:ss
	EndTime      string `json:"endTime"`      // 结束时间，格式为：YYYY-MM-DD HH:mm:ss
	Alarm        uint64 `json:"alarm"`        // 报警标志，0-31位见JTT808协议中定义，32-63位见JTT1078协议中定义
	ResourceType byte   `json:"resourceType"` // 资源类型，0：音视频，1：音频，2：视频,3:视频或音频
	StreamType   byte   `json:"streamType"`   // 码流类型，0：所有码流，1：主码流，2：子码流
	StorageType  byte   `json:"storageType"`  // 存储器类型，0：所有存储器，1：主存储器，2：灾备存储器
}

// 音视频文件上传请求
type AudioVideoFileUploadRequest struct {
	Ip           string `json:"ip"`           // 服务器IP地址
	FtpPort      uint16 `json:"ftpPort"`      // FTP端口号
	UserName     string `json:"userName"`     // 用户名
	Password     string `json:"password"`     // 密码
	Path         string `json:"path"`         // 文件上传路径
	Channel      byte   `json:"channel"`      // 通道号
	StartTime    string `json:"startTime"`    // 开始时间，格式为：YYYY-MM-DD HH:mm:ss
	EndTime      string `json:"endTime"`      // 结束时间，格式为：YYYY-MM-DD HH:mm:ss
	Alarm        uint64 `json:"alarm"`        // 报警标志，0-31位见JTT808协议中定义，32-63位见JTT1078协议中定义，0表示不指定是否有报警
	ResourceType byte   `json:"resourceType"` // 资源类型，0：音视频，1：音频，2：视频,3:视频或音频
	StreamType   byte   `json:"streamType"`   // 码流类型，0：所有码流，1：主码流，2：子码流
	StorageType  byte   `json:"storageType"`  // 存储器类型，0：所有存储器，1：主存储器，2：灾备存储器
	Condition    byte   `json:"condition"`    // 任务执行条件，用bit位表示：bit0：WIFI，bit1：LAN，bit2：3G/4G
}

// 音视频文件上传控制
type AudioVideoFileUploadControlRequest struct {
	PlateformSerial uint16 `json:"plateformSeial"` // 平台消息流水号
	Cmd             byte   `json:"cmd"`            // 控制命令，0：暂停，1：继续，2：取消
}

// 报警附件上传指令
type AIAlarmFileUploadRequest struct {
	Ip               string      `json:"ip"`               // 附件服务器IP地址
	PortTcp          uint16      `json:"portTcp"`          // 附件服务器TCP端口号
	PortUdp          uint16      `json:"portUdp"`          // 附件服务器UDP端口号
	AlarmSignNo      AlarmSignNo `json:"alarmSignNo"`      // 报警标识号
	AlarmPlateformNo string      `json:"alarmPlateformNo"` // 平台报警编号（平台给报警分配的唯一编号）
}

// 报警标识号
type AlarmSignNo struct {
	TerminalId      string `json:"terminalId"`      // 终端ID，7个字节
	Time            string `json:"time"`            // 时间，格式为：YYYY-MM-DD HH:mm:ss
	Sequence        byte   `json:"sequence"`        // 序号，同一时间点报警的序号，从0循环累加
	AttachFileCount byte   `json:"attachFileCount"` // 附件数量，表示该报警对应的附件数量
	Reserved        byte   `json:"reserved"`        // 预留
}

// AI文件上传完成消息应答
type AIAlarmFileUploadCompleteRequest struct {
	FileName  string                          `json:"fileName"`  // 文件名称
	FileType  byte                            `json:"fileType"`  // 文件类型，0：图片，1：音频，2：视频，3：文本，4：其他
	Result    byte                            `json:"result"`    // 结果，0：成功，1：需要补传
	DataItems []AIAlarmFileUploadCompleteItem `json:"dataItems"` // 数据项，需要补传的文件数据项，不需要补传时该字段为空
}

type AIAlarmFileUploadCompleteItem struct {
	DataOffset uint32 `json:"dataOffset"` // 数据偏移量
	DataLength uint32 `json:"dataLength"` // 数据长度
}

// 云台旋转
type PTZRotateRequest struct {
	Channel   byte `json:"channel"`   // 通道号
	Direction byte `json:"direction"` // 方向，0：停止，1：上，2：下，3：左，4：右
	Speed     byte `json:"speed"`     // 速度，0-255
}

// 云台调整焦距控制
type PTZFocalDistanceRequest struct {
	Channel byte `json:"channel"` // 通道号
	Action  byte `json:"action"`  // 焦距调整方向，0：焦距调大，1：焦距调小
}

// 云台调整光圈控制
type PTZApertureRequest struct {
	Channel byte `json:"channel"` // 通道号
	Action  byte `json:"action"`  // 光圈调整方式，0：调大，1：调小
}

// 云台雨刷控制
type PTZWindshieldWiperRequest struct {
	Channel byte `json:"channel"` // 通道号
	Action  byte `json:"action"`  // 启停标识，0：停止，1：启动
}

// 云台红外不光控制
type PTZInfraredRequest struct {
	Channel byte `json:"channel"` // 通道号
	Action  byte `json:"action"`  // 启停标识，0：停止，1：启动
}

// 云台变倍控制
type PTZZoomRequest struct {
	Channel byte `json:"channel"` // 通道号
	Action  byte `json:"action"`  // 变倍控制，0：调大，1：调小
}
