package jtt808client

import (
	"encoding/json"
	"net"
)

var clientBuffer = make([]byte, 0) // 读取缓冲区

// 处理网关服务器发送的数据
func handleServer(conn net.Conn) {
	defer func() {
		if r := recover(); r != nil {
			return
		}
	}()
	defer conn.Close()
	// 定义一个缓冲区，存储读取到的数据
	buf := [BufferSize]byte{}
	for {
		n, err := conn.Read(buf[:])
		if err != nil {
			// 连接关闭，退出循环
			Client().connectStatus = false
			return
		}
		onReceive(conn, buf[:n])
	}
}

func onReceive(conn net.Conn, data []byte) {
	// 将数据添加到缓冲区
	clientBuffer = append(clientBuffer, data...)
	// 分包处理
	onPackege(conn, clientBuffer)
}

func onPackege(conn net.Conn, buffer []byte) {
	// 缓冲区最小长度为8及以下，直接返回
	if len(buffer) <= 8 {
		return
	}
	// 遍历缓冲区，查找开始和结束标志位
	startIndex := 0
	endindIndex := 0
	hasStartIndex := false
	for i := 0; i < len(buffer); i++ {
		// 判断是否找到开始标志位,如果找到开始标志位，继续查找结束标志位
		if hasStartIndex {
			// 判断是否找到结束标志位
			frameEnd := ReadUint32(buffer, &i)
			i -= 4
			if frameEnd == GFrameEnd {
				endindIndex = i
				break
			}
			continue
		}
		frameHeader := ReadUint32(buffer, &i)
		i -= 4
		if frameHeader == GFrameHeader {
			startIndex = i
			hasStartIndex = true
			continue
		}
	}
	// 判断是否找到完整数据帧
	if endindIndex > startIndex {
		// 数据解析，去掉头尾标识位
		decode(conn, buffer[startIndex+4:endindIndex])
		// 删除已经处理的数据帧
		if len(clientBuffer) > endindIndex+4 {
			clientBuffer = clientBuffer[endindIndex+4:]
		} else {
			clientBuffer = []byte{}
		}
		// 判断缓冲区是否为空, 不为空则继续处理分包
		if len(clientBuffer) > 0 {
			onPackege(conn, clientBuffer)
		}
		return
	}
}

// 解码器
func decode(conn net.Conn, data []byte) (err error) {
	// 解析json数据
	gatewayData := &GatewayData{}
	err = json.Unmarshal(data, gatewayData)
	if err != nil {
		return
	}
	// 先处理消息
	if handler, ok := MsgEncodeFuncMap[gatewayData.MsgType]; ok {
		handler(conn, gatewayData)
	}
	// 回调，通知客户端
	if client.Event != nil {
		client.Event.Notify(gatewayData)
	}

	return
}
